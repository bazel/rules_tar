visibility("//tar/...")

DOC = """Filters an archive members.

```py
# Remove all `*.txt` files
tar_filter(
    name = "filtered.tar",
    src = ":archive.tar",
    patterns = ["**/*.txt"],
)
```

Filters can be negated:

```py
# Keep all `.md` files but remove everything else
tar_filter(
    name = "filtered.tar.zst",
    src = ":archive.tar.xz",
    compress = "@rules_tar//tar/compress:zstd",
    patterns = ["**/*.md", "!**/*"],
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "Archive to filter.",
        mandatory = True,
        allow_single_file = [".tar", ".tar.gz", ".tar.bz2", ".tar.zst", "tar.xz"],
    ),
    "compress": attr.label(
        doc = "A compression binary.",
        executable = True,
        cfg = "exec",
    ),
    "patterns": attr.string_list(
        doc = "Glob patterns of archive member names to remove. Can be negated with `!`.",
        mandatory = True,
        allow_empty = False,
    ),
    "_tool": attr.label(
        doc = "The `tar` tool.",
        default = "//tar/tool",
        executable = True,
        cfg = "exec",
    ),
}

def implementation(ctx):
    archive = ctx.actions.declare_file(ctx.label.name)

    output = archive.path
    compress = ctx.executable.compress
    tools = ()
    if compress:
        output = "{}:{}".format(output, compress.path)
        tools = (compress,)

    srcs = depset([ctx.file.src])
    args = ctx.actions.args()
    args.add("--output", output)
    args.add_all(ctx.attr.patterns, format_each = "--filter=%s")
    args.add_all(srcs)

    ctx.actions.run(
        outputs = [archive],
        inputs = srcs,
        arguments = [args],
        executable = ctx.executable._tool,
        tools = tools,
        mnemonic = "TarFilter",
        progress_message = "Filtering %{input} to %{output}",
    )

    return DefaultInfo(files = depset([archive]))

tar_filter = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
)

filter = tar_filter
