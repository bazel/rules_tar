package skip

import (
	"io"
)

type readPeekDiscarder interface {
	io.Reader
	Peek(n int) ([]byte, error)
	Discard(n int) (int, error)
}

func Seek(f readPeekDiscarder) error {
	for {
		buf, err := f.Peek(512)

		if err == io.EOF {
			return err
		}

		if err != nil {
			return err
		}

		for i, b := range buf {
			if b != 0 {
				if _, err := f.Discard(i); err != nil {
					return err
				}
				return nil
			}
		}

		if _, err := f.Discard(512); err != nil {
			return err
		}
	}

	return nil
}
