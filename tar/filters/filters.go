package filters

import (
	"fmt"
	"strings"

	"github.com/bmatcuk/doublestar/v4"
)

type Flag struct {
	filters []string
}

func NewFlag() Flag {
	return Flag{}
}

func (f *Flag) Set(value string) error {
	if !doublestar.ValidatePattern(strings.TrimPrefix(value, "!")) {
		return fmt.Errorf("Invalid filter: %s", value)
	}
	f.filters = append(f.filters, value)
	return nil
}

func (f Flag) String() string {
	return strings.Join(f.filters, ",")
}

func (f Flag) Match(name string) (bool, error) {
	for _, filter := range f.filters {
		after, found := strings.CutPrefix(filter, "!")
		match, err := doublestar.Match(after, name)
		if err != nil {
			return match, err
		}
		if match {
			return found, err
		}
	}
	return true, nil
}
