load("//tar/pipe:subrule.bzl", _ATTRS = "ATTRS", _tar = "implementation", _toolchains = "toolchains")

visibility("//tar/...")

DOC = """Extracts files from a tarball

```py
tar_extract(
    name = "extracted",
    src = ":archive.tar",
    outs = [
        "some/member.txt",
        "some/other/member.txt",
    ]
)
```

The `outs` labels `:some/member.txt` and `:some/other/member.txt` can be used in subsequent rules.
"""

ATTRS = _ATTRS | {
    "src": attr.label(
        doc = "An archive to extract decleared files from.",
        mandatory = True,
        allow_single_file = [".tar", ".tar.gz", ".tar.bz2", ".tar.zst", "tar.xz"],
    ),
    "outs": attr.output_list(
        doc = "Paths within the archive to extract.",
        mandatory = True,
        allow_empty = False,
    ),
}

def _name(label):
    return label.name

def implementation(ctx):
    # TODO: switch this to a subrule, when available
    tar, tools = _tar(ctx, ctx.file.src, _template = ctx.file._template)

    args = ctx.actions.args()
    args.add("-C").add("{}/{}".format(ctx.bin_dir.path, ctx.label.package))
    args.add("-x")
    args.add_all(ctx.attr.outs, map_each = _name)

    ctx.actions.run(
        outputs = ctx.outputs.outs,
        inputs = [ctx.file.src],
        arguments = [args],
        executable = tar,
        tools = tools,
        mnemonic = "TarExtract",
        progress_message = "extracting %{input} files",
        toolchain = "//tar/toolchain/tar:type",
    )

    return DefaultInfo(files = depset(ctx.outputs.outs))

tar_extract = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = _toolchains,
)

extract = tar_extract
