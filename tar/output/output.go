package output

import (
	"os"
	"path"
	"strings"

	"gitlab.arm.com/bazel/rules_tar/v1/tar/compress"
)

type output interface {
	Write(b []byte) (int, error)
	Close() error
}

func newFile(value string) (output, error) {
	if !path.IsAbs(value) {
		if bwd, ok := os.LookupEnv("BUILD_WORKING_DIRECTORY"); ok {
			value = path.Join(bwd, value)
		}
	}

	f, err := os.OpenFile(value, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return nil, err
	}

	return f, nil
}

type stdout struct{}

func newStdout() output {
	return stdout{}
}

func (i stdout) Write(b []byte) (int, error) {
	return os.Stdout.Write(b)
}

func (i stdout) Close() error {
	return nil
}

func newOutput(value string) (output, error) {
	if value == "-" {
		return newStdout(), nil
	}

	o, err := newFile(value)
	if err != nil {
		return nil, err
	}

	return o, nil
}

type Flag struct {
	o output
	v string
}

func NewFlag() Flag {
	return Flag{newStdout(), "-"}
}

func (f Flag) Write(b []byte) (int, error) {
	return f.o.Write(b)
}

func (f Flag) Close() error {
	return f.o.Close()
}

func (f *Flag) Set(value string) error {
	value, binary, found := strings.Cut(value, ":")

	o, err := newOutput(value)
	if err != nil {
		return err
	}

	if found {
		o, err = compress.NewSubprocess(o, binary)
	} else {
		o, err = compress.NewExtension(o, path.Ext(value))
	}

	if err != nil {
		return err
	}

	f.o = o
	f.v = value

	return nil
}

func (f Flag) String() string {
	return f.v
}
