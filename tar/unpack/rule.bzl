load("//tar/pipe:subrule.bzl", _ATTRS = "ATTRS", _tar = "implementation", _toolchains = "toolchains")

visibility("//tar/...")

DOC = """unpacks files from a tarball

```py
tar_unpack(

    name = "extracted",
    src = ":archive.tar",
)
```
"""

ATTRS = _ATTRS | {
    "src": attr.label(
        doc = "An archive to extract into a declared directory (`TreeArtifact`).",
        mandatory = True,
        allow_single_file = [".tar", ".tar.gz", ".tar.bz2", ".tar.zst", "tar.xz"],
    ),
}

def implementation(ctx):
    # TODO: switch this to a subrule, when available
    tar, tools = _tar(ctx, ctx.file.src, _template = ctx.file._template)

    output_dir = ctx.actions.declare_directory(ctx.label.name)

    args = ctx.actions.args()
    args.add("-x")
    args.add("-C").add(output_dir.path)

    ctx.actions.run(
        outputs = [output_dir],
        inputs = [ctx.file.src],
        arguments = [args],
        executable = tar,
        tools = tools,
        mnemonic = "TarUnpack",
        progress_message = "unpacking %{input}",
    )

    depset_files = depset([output_dir])
    return DefaultInfo(files = depset_files)

tar_unpack = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = _toolchains,
)

unpack = tar_unpack
