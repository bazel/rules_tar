visibility("//tar/...")

DOC = """Concatenates multiple tape archives together.

```py
tar_concatenate(
    name = "merged.tar",
    srcs = [":archive.tar", "archive.tar.gz"]
)
```
"""

ATTRS = {
    "srcs": attr.label_list(
        doc = "Archives to concatenate together.",
        mandatory = True,
        allow_empty = False,
        allow_files = [".tar", ".tar.gz", ".tar.bz2", ".tar.zst", "tar.xz"],
    ),
    "compress": attr.label(
        doc = "A compression binary.",
        executable = True,
        cfg = "exec",
    ),
    "duplicate": attr.string(
        doc = "Behaviour when duplicate paths are detected.",
        default = "error",
        values = ["error", "skip", "continue"],
    ),
    "_tool": attr.label(
        doc = "The `tar` tool.",
        default = "//tar/tool",
        executable = True,
        cfg = "exec",
    ),
}

def implementation(ctx):
    archive = ctx.actions.declare_file(ctx.label.name)

    output = archive.path
    compress = ctx.executable.compress
    tools = ()
    if compress:
        output = "{}:{}".format(output, compress.path)
        tools = (compress,)

    srcs = depset(transitive = [s[DefaultInfo].files for s in ctx.attr.srcs])
    args = ctx.actions.args()
    args.add("--output", output)
    args.add("--duplicate", ctx.attr.duplicate)
    args.add_all(srcs)

    ctx.actions.run(
        outputs = [archive],
        inputs = srcs,
        arguments = [args],
        executable = ctx.executable._tool,
        tools = tools,
        mnemonic = "TarConcatenate",
        progress_message = "Concatenating into %{output}",
    )

    return DefaultInfo(files = depset([archive]))

tar_concatenate = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
)

concatenate = tar_concatenate
