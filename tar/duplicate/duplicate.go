package duplicate

import (
	"fmt"
)

type Flag int

const (
	Error    Flag = 0
	Skip     Flag = 1
	Continue Flag = 2
)

func NewFlag() Flag {
	return Error
}

func (f *Flag) Set(value string) error {
	switch value {
	case "error":
		*f = Error
	case "skip":
		*f = Skip
	case "continue":
		*f = Continue
	default:
		return fmt.Errorf("Invalid duplicate value: %s", value)
	}
	return nil
}

func (f Flag) String() string {
	switch f {
	case Error:
		return "error"
	case Skip:
		return "skip"
	case Continue:
		return "continue"
	}
	return "unknown"
}
