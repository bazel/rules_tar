package input

import (
	"bufio"
	"io"
	"os"
	"path"
	"syscall"

	"gitlab.arm.com/bazel/rules_tar/v1/tar/decompress"
)

type readPeekDiscardCloser interface {
	io.ReadCloser
	Peek(n int) ([]byte, error)
	Discard(n int) (int, error)
}

type file struct {
	f *os.File
	b *bufio.Reader
}

func newFile(f *os.File) (readPeekDiscardCloser, error) {
	reader, err := decompress.NewMagic(f)
	if err != nil {
		return nil, err
	}
	return file{f, reader}, nil
}

func (i file) Read(b []byte) (int, error) {
	return i.b.Read(b)
}

func (i file) Peek(n int) ([]byte, error) {
	return i.b.Peek(n)
}

func (i file) Discard(n int) (int, error) {
	return i.b.Discard(n)
}

func (i file) Close() error {
	return i.f.Close()
}

type stdin struct {
	b *bufio.Reader
}

func newStdin() readPeekDiscardCloser {
	return stdin{bufio.NewReader(os.Stdin)}
}

func (i stdin) Read(b []byte) (int, error) {
	return i.b.Read(b)
}

func (i stdin) Peek(n int) ([]byte, error) {
	return i.b.Peek(n)
}

func (i stdin) Discard(n int) (int, error) {
	return i.b.Discard(n)
}

func (i stdin) Close() error {
	return nil
}

func Open(v string) (readPeekDiscardCloser, error) {
	if v == "-" {
		return newStdin(), nil
	}

	f, err := os.Open(v)

	switch e := err.(type) {
	case *os.PathError:
		if e.Err == syscall.ENOENT {
			if bwd, ok := os.LookupEnv("BUILD_WORKING_DIRECTORY"); ok {
				f, err = os.Open(path.Join(bwd, v))
			}
		}
	}

	if err != nil {
		return nil, err
	}

	return newFile(f)
}
