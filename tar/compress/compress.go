package compress

import (
	"compress/gzip"
	"fmt"
	"io"
	"os/exec"

	"github.com/dsnet/compress/bzip2"
	"github.com/klauspost/compress/zstd"
	"github.com/ulikunitz/xz"
)

type subprocess struct {
	stdin io.WriteCloser
	wait  chan error
}

func NewSubprocess(w io.Writer, value string) (io.WriteCloser, error) {
	cmd := exec.Command(value)
	cmd.Stdout = w

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, err
	}

	if err = cmd.Start(); err != nil {
		return nil, err
	}

	wait := make(chan error, 1)
	go func() {
		wait <- cmd.Wait()
	}()

	return subprocess{stdin, wait}, nil
}

func (s subprocess) Write(b []byte) (int, error) {
	return s.stdin.Write(b)
}

func (s subprocess) Close() error {
	if err := s.stdin.Close(); err != nil {
		return err
	}

	if err := <-s.wait; err != nil {
		return err
	}

	return nil
}

type writeCloser struct {
	wrapped  io.Closer
	compress io.WriteCloser
}

func (w writeCloser) Write(b []byte) (int, error) {
	return w.compress.Write(b)
}

func (w writeCloser) Close() error {
	if err := w.compress.Close(); err != nil {
		return err
	}
	return w.wrapped.Close()
}

type passthrough struct {
	w io.Writer
}

func (p passthrough) Write(b []byte) (int, error) {
	return p.w.Write(b)
}

func (w passthrough) Close() error {
	return nil
}

func extension(w io.WriteCloser, ext string) (io.WriteCloser, error) {
	switch ext {
	case ".tar":
		return passthrough{w}, nil
	case ".gz", ".tar.gz", ".tgz":
		return gzip.NewWriterLevel(w, gzip.BestCompression)
	case ".bz2", ".tar.bz2":
		return bzip2.NewWriter(w, nil)
	case ".xz", ".tar.xz":
		return xz.NewWriter(w)
	case ".zst", ".tar.zst":
		return zstd.NewWriter(w)
	default:
		return nil, fmt.Errorf("unknown extension `%s`", ext)
	}
}

func NewExtension(w io.WriteCloser, ext string) (io.WriteCloser, error) {
	c, err := extension(w, ext)
	if err != nil {
		return nil, err
	}

	return writeCloser{w, c}, nil
}
