package main

import (
	"archive/tar"
	"flag"
	"io"
	"log"

	"gitlab.arm.com/bazel/rules_tar/v1/tar/duplicate"
	"gitlab.arm.com/bazel/rules_tar/v1/tar/filters"
	"gitlab.arm.com/bazel/rules_tar/v1/tar/input"
	"gitlab.arm.com/bazel/rules_tar/v1/tar/output"
	"gitlab.arm.com/bazel/rules_tar/v1/tar/skip"
)

type Flags struct {
	Output    output.Flag
	Duplicate duplicate.Flag
	Filters   filters.Flag
}

func main() {
	flags := Flags{output.NewFlag(), duplicate.NewFlag(), filters.NewFlag()}
	flag.Var(&flags.Output, "output", "Output location for concatenated `tar`.")
	flag.Var(&flags.Duplicate, "duplicate", "Operation when duplicate archive member names are encountered.")
	flag.Var(&flags.Filters, "filter", "A glob pattern to filter archive members.")
	flag.Parse()
	defer flags.Output.Close()

	w := tar.NewWriter(flags.Output)
	defer w.Close()

	args := flag.Args()
	if len(args) == 0 {
		args = []string{"-"}
	}

	paths := make(map[string]struct{})
	for _, archive := range args {
		f, err := input.Open(archive)
		if err != nil {
			log.Fatalf("failed to open archive `%s`: %s", archive, err)
		}
		defer f.Close()

		for {
			r := tar.NewReader(f)
			for {
				h, err := r.Next()
				if err == io.EOF {
					break
				}
				if err != nil {
					log.Fatalf("failed to advance to next archive `%s` member: %s", archive, err)
				}

				match, err := flags.Filters.Match(h.Name)
				if err != nil {
					log.Fatalf("failed to match filter on `%s#%s`: %s", archive, h.Name, err)
				}
				if !match {
					continue
				}

				if _, exists := paths[h.Name]; exists {
					switch flags.Duplicate {
					case duplicate.Error:
						log.Fatalf("name already exists in a previous archive: %s", h.Name)
					case duplicate.Skip:
						continue
					case duplicate.Continue:
					default:
						log.Fatalf("unknown duplicate handling: %d", flags.Duplicate)
					}
				}
				paths[h.Name] = struct{}{}
				if err := w.WriteHeader(h); err != nil {
					log.Fatalf("failed to write header for `%s#%s`: %s", archive, h.Name, err)
				}
				if _, err := io.Copy(w, r); err != nil {
					log.Fatalf("failed to copy member for `%s#%s`: %s", archive, h.Name, err)
				}
			}

			err := skip.Seek(f)
			if err == io.EOF {
				break
			} else if err == nil {
				continue
			}
			log.Fatalf("failed to skip to next archive: %s", err)
		}

		if err := f.Close(); err != nil {
			log.Fatalf("failed to close archive `%s`: %s", archive, err)
		}
	}

	if err := w.Close(); err != nil {
		log.Fatalf("failed to close writer: %s", err)
	}

	if err := flags.Output.Close(); err != nil {
		log.Fatalf("failed to close output stream: %s", err)
	}
}
