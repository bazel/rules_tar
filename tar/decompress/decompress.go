package decompress

import (
	"bufio"
	"compress/gzip"
	"fmt"
	"io"

	"github.com/dsnet/compress/bzip2"
	"github.com/h2non/filetype"
	"github.com/klauspost/compress/zstd"
	"github.com/ulikunitz/xz"
)

func NewMagic(r io.Reader) (*bufio.Reader, error) {
	reader := bufio.NewReader(r)

	if buf, err := reader.Peek(3); err == nil && filetype.IsExtension(buf, "gz") {
		decompressor, err := gzip.NewReader(reader)
		if err != nil {
			return nil, err
		}
		return bufio.NewReader(decompressor), err
	}

	if buf, err := reader.Peek(3); err == nil && filetype.IsExtension(buf, "bz2") {
		decompressor, err := bzip2.NewReader(reader, nil)
		if err != nil {
			return nil, err
		}
		return bufio.NewReader(decompressor), err
	}

	if buf, err := reader.Peek(6); err == nil && filetype.IsExtension(buf, "xz") {
		decompressor, err := xz.NewReader(reader)
		if err != nil {
			return nil, err
		}
		return bufio.NewReader(decompressor), err
	}

	if buf, err := reader.Peek(4); err == nil && filetype.IsExtension(buf, "zst") {
		decompressor, err := zstd.NewReader(reader)
		if err != nil {
			return nil, err
		}
		return bufio.NewReader(decompressor), err
	}

	if buf, err := reader.Peek(262); err == nil && filetype.IsExtension(buf, "tar") {
		return reader, err
	}

	kind, err := filetype.MatchReader(reader)
	if err != nil {
		return nil, err
	}

	if kind.MIME.Value == "" {
		kind.MIME.Value = "unknown"
	}

	return nil, fmt.Errorf("unknown file `%s`", kind.MIME.Value)
}
