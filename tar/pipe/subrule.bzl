visibility("//...")

DOC = """Provides a `tar` binary that can perform decompression based on an input file.

```py
def implementation(ctx):
    tar, tools = tar_pipe(ctx.file.src)
    # Pass `tar`/`tools` to `ctx.actions.run`

something = rule(
  implementation = implementation,
  subrules = [tar_pipe],
)
```
"""

ATTRS = {
    "_template": attr.label(
        doc = "The pipe template.",
        allow_single_file = True,
        default = ":template",
    ),
}

toolchains = (
    "//tar/toolchain/tar:type",
    "@rules_zstd//zstd/toolchain/zstd:type",
    "@rules_bzip2//bzip2/toolchain/bzip2:type",
    "@rules_gzip//gzip/toolchain/gzip:type",
    "@rules_xz//xz/toolchain/xz:type",
)

MAP = {
    "tar": None,
    "gz": "@rules_gzip//gzip/toolchain/gzip:type",
    "bz2": "@rules_bzip2//bzip2/toolchain/bzip2:type",
    "xz": "@rules_xz//xz/toolchain/xz:type",
    "zst": "@rules_zstd//zstd/toolchain/zstd:type",
}

def implementation(ctx, src, *, _template):
    tar = ctx.toolchains["//tar/toolchain/tar:type"]
    tools = (tar.run,)

    substitutions = ctx.actions.template_dict()
    substitutions.add("{{tar}}", tar.executable.path)
    substitutions.add("{{src}}", src.path)
    kind = MAP[src.extension]
    if kind != None:
        decompress = ctx.toolchains[kind]
        tools += (decompress.run,)
        substitutions.add("{{decompress}}", decompress.executable.path)

    rendered = ctx.actions.declare_file("{}.tar.pipe/tar.{}".format(ctx.label.name, _template.extension))
    ctx.actions.expand_template(
        template = _template,
        output = rendered,
        computed_substitutions = substitutions,
        is_executable = True,
    )

    return rendered, tools

# TODO: enable `subrule` when they become non-experimental, use `bazel_features` to detect
#tar_pipe = subrule(
#    # doc = DOC,
#    implementation = implementation,
#    attrs = ATTRS,
#    toolchains = toolchains,
#)

#pipe = tar_pipe
