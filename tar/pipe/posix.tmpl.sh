#! /usr/bin/env sh

# Strict Shell
set -o errexit
set -o nounset

# Bazel substitutions
TAR="{{tar}}"
DECOMPRESS="{{decompress}}"
SRC="{{src}}"
readonly TAR DECOMPRESS SRC

# Run `tar`
if test ! -x "${DECOMPRESS}"; then
  "${TAR}" -f "${SRC}" "${@}"
  exit 0
fi

# Workaround no POSIX `pipefail`
readonly FAIL=".fail.$$"
{ "${DECOMPRESS}" -fcd "${SRC}" || echo "$?" >"${FAIL}"; } | "${TAR}" "${@}"
if test ! -s "${FAIL}"; then
  exit 0
fi

while IFS= read -r STATUS; do
  exit "${STATUS}"
done <"${FAIL}"

printf >&2 'Pipefail workaround failed\n'
exit 1
