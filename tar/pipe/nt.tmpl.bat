@echo off

:: Enable Batch extensions
verify other 2>nul
setlocal EnableExtensions
if errorlevel 1 (
  echo>&2.Failed to enable extensions
  exit /b 120
)

:: Bazel substitutions
for /f usebackq %%a in ('{{tar}}') do set "TAR=%%~fa"
for /f usebackq %%a in ('{{src}}') do set "SRC=%%~a"
for /f usebackq %%a in ('{{decompress}}') do set "DECOMPRESS=%%~a"

:: Run `tar`
if not exist "%DECOMPRESS%" (
  "%TAR%" -f "%SRC%" %*
  exit /b "%ERRORLEVEL%"
)

:: Convert `DECOMPRESS` into a Windows path to allow execution
for /f usebackq %%a in ('%DECOMPRESS%') do set "DECOMPRESS=%%~fa"

:: Run the decompression
"%DECOMPRESS%" -cd "%SRC%" | "%TAR%" %*
exit /b "%ERRORLEVEL%"