# `rules_tar`

> A Bazel ruleset for hermetically working with tape archives (`tar`)

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(name="rules_tar", version="0.0.0")
```

Use the unpacking and extraction rules:

```py
load("@rules_tar//tar/concatenate:defs.bzl", "tar_concatenate")
load("@rules_tar//tar/filter:defs.bzl", "tar_filter")
load("@rules_tar//tar/extract:defs.bzl", "tar_extract")
load("@rules_tar//tar/unpack:defs.bzl", "tar_unpack")

tar_unpack(
    name = "unpack",
    src = "some.tar.bz2",
)

tar_extract(
    name = "extract",
    src = "some.tar.zst",
    outs = [
        "a/specific/member.txt",
        "another/member.txt",
    ]
)

tar_concatenate(
    name = "concatenate.tar.zst",
    srcs = [
        "some.tar.gz",
        "someother.tar.bzip2",
        "something.tar.zst",
    ],
)

tar_filter(
    name = "filtered.tar.zst",
    srcs = "some.tar.xz",
    patterns = [
        "**/*.txt",  # Remove all `.txt` members
    ],
)
```

> Note: use [rules_pkg] to pack archives.
>
> This ruleset _may_ support hermetic packing without a Python toolchain in the future.

# Hermeticity

The module is entirely hermetic, using `@ape//ape/toolchain/info:tar` as the default toolchain.

## Release Registry

The project publishes the relevant files to GitLab releases for use when a version has not been added to the upstream [BCR][bcr].

This is often the case for pre-release versions.

Add the following to `.bazelrc`:

```
# `bzlmod` pre-release registries
common --registry https://bcr.bazel.build
common --registry=https://gitlab.arm.com/bazel/rules_tar/-/releases/v1.0.0-alpha.1/downloads
```

Then a GitLab release version can be used in `bazel_dep`.

[bcr]: https://registry.bazel.build/
[rules_pkg]: https://github.com/bazelbuild/rules_pkg
