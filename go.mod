module gitlab.arm.com/bazel/rules_tar/v1

go 1.21.3

require (
	github.com/bmatcuk/doublestar/v4 v4.6.1
	github.com/h2non/filetype v1.1.3
	github.com/klauspost/compress v1.17.10
	github.com/ulikunitz/xz v0.5.12
	github.com/dsnet/compress v0.0.1
)

